package com.example.demo;

import com.example.demo.design_patterns.creational.FactoryMethod;
import com.example.demo.dto.Animal;
import com.example.demo.dto.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

    @Autowired
    private Animal cat;

    @Autowired
    private Animal cat1;

    @Autowired
    private Animal cat2;

    @Autowired
    private Animal cat3;

    @Autowired
    private Animal cat4;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Cat : " + cat);
        System.out.println("Cat : " + cat1);
    }
}

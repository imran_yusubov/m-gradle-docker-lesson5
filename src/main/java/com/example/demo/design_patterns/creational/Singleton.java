package com.example.demo.design_patterns.creational;

//one and only one instance of the given class
// prototype, always new instance
public class Singleton {

    private static Singleton instance;

    public static Singleton getInstance() {
        if (instance == null)
            synchronized (Singleton.class) {
                if (instance == null)
                    instance = new Singleton();
            }
        return instance;
    }

    private Singleton() {
    }
}

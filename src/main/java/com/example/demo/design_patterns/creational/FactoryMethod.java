package com.example.demo.design_patterns.creational;

import com.example.demo.dto.Animal;
import com.example.demo.dto.Dog;

public class FactoryMethod {

    private static int count;

    public static Animal getInstance() {
        count++;
        return new Dog();
    }
}

package com.example.demo.bean;

import com.example.demo.dto.Animal;
import com.example.demo.dto.Dog;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class DesignPatternConfig {

    @Bean
    @Scope("prototype")
    public Animal getCat() {
        return new Dog();
    }
}
